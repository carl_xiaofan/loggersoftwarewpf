﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LoggerSoftware.ViewModels;
using LoggerSoftware.Models;

namespace LoggerSoftware.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
        }

        private void TextBox_Terminal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            tb.CaretIndex = tb.Text.Length;
            tb.ScrollToEnd();

            if (ScrollViewer_Terminal != null)
                ScrollViewer_Terminal.ScrollToBottom();
        }

        private void ChannelsDataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            if (ChannelsDataGrid != null && ChannelsDataGrid.Items != null)
            {
                ChannelsDataGrid.Items.Refresh();
            }
        }
    }
}
