﻿using System;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerSoftware.Models;

namespace LoggerSoftware.ViewModels
{
    class SensorSettingViewModel : Conductor<Object>
    {
        private IEventAggregator _eventAggregator;
        public SensorSettingViewModel(IEventAggregator eventAggregator, SensorInfo info, ref Connection conn)
        {
            _eventAggregator = eventAggregator;
            ChannelSensorInfo = new SensorInfo(info);
            _connection = conn;
        }

        private SensorInfo _channelSensorInfo;
        private Connection _connection;
        public SensorInfo ChannelSensorInfo
        {
            get { return _channelSensorInfo; }
            set
            {
                _channelSensorInfo = value;
                NotifyOfPropertyChange(() => ChannelSensorInfo);
            }
        }

        public void PushSettingButtonClick()
        {
           ChannelSensorInfo =  _connection.SendSensorInfo(ChannelSensorInfo);

            _eventAggregator.PublishOnUIThread(ChannelSensorInfo);
        }

        public void ResetSettingButtonClick()
        {
            ChannelSensorInfo.Type = 0;
            ChannelSensorInfo = _connection.SendSensorInfo(ChannelSensorInfo);
            _eventAggregator.PublishOnUIThread(ChannelSensorInfo);
        }
    }
}
