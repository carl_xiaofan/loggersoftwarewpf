﻿using System;
using Caliburn.Micro;
using System.IO.Ports;
using LoggerSoftware.Models;

namespace LoggerSoftware.ViewModels
{
    class ConnectionViewModel : Conductor<Object>
    {
        private LoggerInfo loggerInfo;
        private Connection connection;
        private IEventAggregator _eventAggregator;

        public ConnectionViewModel(IEventAggregator eventAggregator, ref LoggerInfo info, ref Connection conn)
        {
            _eventAggregator = eventAggregator;

            ScanPorts();
            loggerInfo = info;
            connection = conn;
        }

        #region BindVariable
        private BindableCollection<string> _ports;
        private string _selectedPort;
        private int _selectedMethod;
        private int _inputSerial;

        public BindableCollection<string> Ports
        {
            get { return _ports; }
            set
            {
                _ports = value;
                NotifyOfPropertyChange(() => Ports);
            }
        }

        public string SelectedPort
        {
            get { return _selectedPort; }
            set
            {
                _selectedPort = value;
                NotifyOfPropertyChange(() => SelectedPort);
            }
        }

        public int SelectedMethod
        {
            get { return _selectedMethod; }
            set
            {
                _selectedMethod = value;
                NotifyOfPropertyChange(() => SelectedMethod);
            }
        }

        public int InputSerial
        {
            get { return _inputSerial; }
            set
            {
                _inputSerial = value;
                NotifyOfPropertyChange(() => InputSerial);
            }
        }
        #endregion //Binding Variable

        public void ScanPorts()
        {
            _ports = new BindableCollection<string>(SerialPort.GetPortNames());
            if(_ports.Count > 0)
            {
                _selectedPort = _ports[0];
                _selectedMethod = 0;
            }
        }

        public void EstablishConnection()
        {
            switch (_selectedMethod)
            {
                case 0:
                    connection = new Connection(_selectedPort, 0, _inputSerial, ref loggerInfo);
                    break;
                case 1:
                    connection = new Connection(_selectedPort, 1, _inputSerial, ref loggerInfo);
                    break;
                case 2:
                    connection = new Connection(_selectedPort, 1, _inputSerial, ref loggerInfo);
                    break;
                case 3:
                    connection = new Connection(_selectedPort, 2, _inputSerial, ref loggerInfo);
                    break;
                default:
                    break;
            }

            if (connection.connected)
            {
                connection.Get_RecordInfo();
                connection.Get_RadioInfo();
                connection.Get_RoleInfo();
                connection.Get_Menu();

                _eventAggregator.BeginPublishOnUIThread(connection);
                _eventAggregator.PublishOnUIThread("Connection Successful");
                TryClose();
            }
            else
            {
                _eventAggregator.PublishOnUIThread("Connection Fail");
            }
        }
    }
}
