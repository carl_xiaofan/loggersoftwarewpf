﻿using System;
using System.IO.Ports;
using System.IO;
using Caliburn.Micro;
using LoggerSoftware.Models;
using System.Diagnostics;
using LoggerSoftware.Views;
using System.Threading;

namespace LoggerSoftware.ViewModels
{
	public class ShellViewModel : Conductor<Object>, IHandle<string>, IHandle<Connection>, IHandle<SensorInfo>
	{
		private readonly IEventAggregator _eventAggregator;

		public ShellViewModel(IEventAggregator eventAggregator)
		{
			_eventAggregator = eventAggregator;
			_eventAggregator.Subscribe(this);

			LoggerData = new LoggerInfo();
			SelectedSensorInfo = LoggerData.SensorInfos[0];
			SoftwareData = new SoftwareSettings();
			SoftwareVersion = "V2.0.0";
			DownloadCalibration = false;

			SetSoftwareState("DisConnect");
			AppendLogMesaage("Software Start");
			AppendLogMesaage("Wait for Connection");
		}

		#region BindVariable
		private string _softwareVersion;
		private string _softwareState;
		private string _logMessage;
		private SoftwareSettings _softwareSettings;
		private LoggerInfo _loggerData;
		private SensorInfo _selectedSensorInfo;
		private string _selectedMenu;
		private bool _downloadCalibration;
		private Connection _connection;
		private IWindowManager theWindowManager = new WindowManager();

		public string SoftwareVersion
		{
			get { return _softwareVersion; }
			set
			{
				_softwareVersion = value;
			}
		}

		public string SoftwareState
		{
			get { return _softwareState; }
			set
			{
				_softwareState = value;
				NotifyOfPropertyChange(() => SoftwareState);
			}
		}

		public string LogMessage
		{
			get { return _logMessage; }
			set
			{
				_logMessage = value;
				NotifyOfPropertyChange(() => LogMessage);
			}
		}

		public SoftwareSettings SoftwareData
		{
			get { return _softwareSettings; }
			set
			{
				_softwareSettings = value;
				NotifyOfPropertyChange(() => SoftwareData);
			}
		}

		public LoggerInfo LoggerData
		{
			get { return _loggerData; }
			set
			{
				_loggerData = value;
				NotifyOfPropertyChange(() => LoggerData);
			}
		}

		public SensorInfo SelectedSensorInfo
		{
			get { return _selectedSensorInfo; }
			set
			{
				_selectedSensorInfo = value;
				NotifyOfPropertyChange(() => SelectedSensorInfo);
			}
		}

		public string SelectedMenu
		{
			get { return _selectedMenu; }
			set
			{
				_selectedMenu = value;
				NotifyOfPropertyChange(() => SelectedMenu);
			}
		}

		public bool DownloadCalibration
		{
			get { return _downloadCalibration; }
			set
			{
				_downloadCalibration = value;
				NotifyOfPropertyChange(() => DownloadCalibration);
			}
		}
		#endregion // BindVariable

		#region MenuClickEvent
		public void CoreSettingMenuClick()
		{
			SoftwareData.SetEnableCore();
			if (this.SoftwareData.EnableCoreSettings)
			{
				AppendLogMesaage("Enable Core Settings");
			}
			else
			{
				AppendLogMesaage("Disable Core Settings");
			}
		}

		public void NewConnectionMenuClick()
		{
			var connectionView = new ConnectionViewModel(_eventAggregator, ref _loggerData, ref _connection);
			theWindowManager.ShowWindow(connectionView, null, null);
		}
		#endregion // MenuClickEvent

		public void ChannelSetButtonClick()
		{
			_selectedSensorInfo =  _connection.Get_SensorInfo(_selectedSensorInfo.Channel - 1);

			var sensorSettingView = new SensorSettingViewModel(_eventAggregator, _selectedSensorInfo, ref _connection);
			theWindowManager.ShowWindow(sensorSettingView, null, null);
		}

		public void ChannelResetButtonClick()
		{
			_connection.SendSensorReset();

			for (var i = 0; i < LoggerInfo.MAX_CHANNEL; i++)
			{
				LoggerData.SensorInfos[i] = new SensorInfo();
				LoggerData.SensorInfos[i].Channel = (byte)(i + 1);
			}
		}

		public void RadioRefreshButtonClick()
		{
			_connection.Get_RadioInfo();
		}

		public void RoleRefreshButtonClick()
		{
			_connection.Get_RoleInfo();
		}

		public void IntervalButtonClick()
		{
			_connection.SendInterval();
		}

		public void EnableRecordToggle()
		{
			_connection.SendEnableToggle(1);
		}

		public void EnableSynchronizeToggle()
		{
			_connection.SendEnableToggle(2);
		}

		public void EnableFastReadToggle()
		{
			_connection.SendEnableToggle(3);
		}

		public void RadioSetButtonClick()
		{
			_connection.SendRadioSetting();
		}

		public void RoleSetButtonClick()
		{
			_connection.SendRoleSetting();
		}

		public void DownloadRefreshButtonClick()
		{
			_connection.Get_Menu();
		}

		public void DownloadResetButtonClick()
		{
			_connection.SendMenuReset();

			_connection.Get_Menu();
		}

		public void DownloadDateButtonClick()
		{
			Thread downloadThread = new Thread(DownloadThread);
			downloadThread.Start();
		}

		private void DownloadThread()
		{
			int day = Int32.Parse(SelectedMenu.Split('/')[0]);
			int month = Int32.Parse(SelectedMenu.Split('/')[1]);
			int year = Int32.Parse(SelectedMenu.Split('/')[2]);

			if (DownloadCalibration)
			{
				; _connection.Receive_REAL_LOG(year, month, day, ref _softwareSettings);
			}
			else
			{
				_connection.Receive_RAW_LOG(year, month, day, ref _softwareSettings);
			}
		}

		public void DownloadViewButtonClick()
		{
			string path = Environment.CurrentDirectory + string.Format("\\DownloadData\\{0}", LoggerData.Serial);
			if (Directory.Exists(path))
			{
				Process.Start("explorer.exe", @path);
			}
			else
			{

			}
		}

		public void SyncPCTime()
		{
			this.LoggerData.CurrentTime = DateTime.Now;
			AppendLogMesaage("SyncPCTime to Software");
		}

		private void SetSoftwareState(string state)
		{
			this.SoftwareState = state;
		}

		private void AppendLogMesaage(string message)
		{
			this.LogMessage += "> ";
			this.LogMessage += message;
			this.LogMessage += Environment.NewLine;
		}

		#region Handle
		public void Handle(Connection conn)
		{
			_connection = conn;
		}

		public void Handle(string message)
		{
			switch (message)
			{
				case "Connection Successful":
					SetSoftwareState("Connected");
					AppendLogMesaage("Connection Successful");
					AppendLogMesaage("Logger Basic Infos Updated");
					SyncPCTime();
					SoftwareData.LoggerConnected = true;
					break;
			}
		}

		public void Handle(SensorInfo info)
		{
			LoggerData.SensorInfos[info.Channel - 1] = info;
			SelectedSensorInfo = info;
		}
        #endregion // Handle
    }
}