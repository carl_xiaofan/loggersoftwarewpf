﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    public class Constant
    {
        public static byte START_BYTE = 0xB2;
        public static byte STOP_BYTE = 0xDC;

        public static byte[] START_BYTE_ARY = new byte[] { START_BYTE, START_BYTE };
        public static byte[] STOP_BYTE_ARY = new byte[] { STOP_BYTE, STOP_BYTE };

        public static string[] sensor_types = new string[6] {
            "Unused", "Vibrating Wire", "Resistance", "Voltage", "Frequency", "Counter" };
        public static string[] equation_types = new string[3] {
            "Raw Data", "Polynomial", "SteinhartHart" };

        public enum Serial_flag
        {
            FIN = 0x01,
            SYN = 0x02,
            ACK = 0x04,
            ACK_SYN = 0x06,
            SET = 0x08,
            DAT = 0x10,
            FOR = 0x20,
        };

        public enum Set_flag
        {
            SER = 0x01,
            FWV = 0x02,
            RTC = 0x03,
            BUILD_DATE = 0x04,
            CHECK_DATE = 0x05,
            MODE = 0x06,
            ROLE = 0x07,
            SENSOR_TYPE = 0X08,
            SENESOR_SETTLE = 0X09,
            SENSOR_START = 0X0A,
            SENSOR_STOP = 0x0B,
            SENSOR_CYCLES = 0x0C,
            SENSOR_INTERVAL = 0x0D,
            SENSOR_EQUATION = 0x0E,
            SENSOR_COF = 0x0F,
            SENSOR_COMP = 0x10,
            SENSOR_COMPCHAN = 0x11,
            COUNTER_RESET = 0x12,
            SAMPLE_CHANGE = 0x13,
            SENSOR_UNIT = 0x14,
            RESET_SENSOR = 0x15,
            RESET_SD = 0x16,
            SLAVE_LIST = 0x17,
            SLAVE_NUMBER = 0x18,
            SENSOR = 0x19,
            ENABLE_RECORD = 0x1A,
            RECORD = 0x1B,
            ROLE_INFO = 0x1C,
            SYSTEM_INFO = 0x1D,
            RADIO_HPID = 0x1E,
            ENABLE_SYNCHRONIZE = 0x1F,
            ENABLE_FASTREAD = 0x20,
        };

        public enum Dat_flag
        {
            RTM = 0x01,
            RED = 0x02,
            LST = 0x03,
            SENSOR_INFO = 0x04,
            RECORD_INFO = 0x05,
            SLST = 0x06,
            ROLE_INFO = 0x07,
            RADIO_DAT = 0x08,
        };

        public struct Record
        {
            public int year;
            public int month;
            public int day;
            public int hour;
            public int minute;
            public int second;
            public float[] readings;

            public Record(int y, int m, int d, int h, int mm, int s, float[] record_readings)
            {
                year = y;
                month = m;
                day = d;
                hour = h;
                minute = mm;
                second = s;
                readings = record_readings;
            }
        }
    }
}
