﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    class Calculation
    {
        /* ************************************************************
 * Description : Polynomial Equation
 * Input : coefficient raw_data and offset
 * Output : reading after calculation
 * ************************************************************
 */
        public static float Polynomial(List<float> cof, float rawReading, float offset)
        {
            float result = 0;
            if (offset != 0)
            {
                result += offset;
            }
            else
            {
                result += cof[0];
            }

            if (cof[1] != 0)
            {
                result += cof[1] * rawReading;
            }
            if (cof[2] != 0)
            {
                result += cof[2] * rawReading * rawReading;
            }
            if (cof[3] != 0)
            {
                result += cof[3] * rawReading * rawReading * rawReading;
            }
            if (cof[4] != 0)
            {
                result += cof[4] * rawReading * rawReading * rawReading * rawReading;
            }

            return result;
        }

        /* ************************************************************
         * Description : Steinhart Equation
         * Input : coefficient raw_data and offset
         * Output : reading after calculation
         * ************************************************************
         */
        public static double Steinhart(List<float> cof, float rawReading, float offset)
        {
            double result = 0;
            double s = Math.Log(rawReading);
            if (offset != 0)
            {
                result += offset;
            }
            else
            {
                result += cof[0];
            }
            if (cof[1] != 0)
            {
                result += cof[1] * s;
            }
            if (cof[3] != 0)
            {
                result += cof[3] * s * s * s;
            }

            return result;
        }
    }
}
