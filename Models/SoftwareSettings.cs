﻿using System;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LoggerSoftware.Models
{
    public class SoftwareSettings : Screen
    {

        #region Data Members
        // ----------------------------------------------------------------------
        private bool _enableCoreSettings;
        private bool _loggerConnected;
        private int _progressbarValue;
        // ----------------------------------------------------------------------
        #endregion // Data Members

        #region Constructor
        public SoftwareSettings()
        {
            EnableCoreSettings = false;
            LoggerConnected = false;
        }
        #endregion // Constructor

        #region Properties
        public bool EnableCoreSettings
        {
            get
            {
                return _enableCoreSettings;
            }
            set
            {
                _enableCoreSettings = value;
                NotifyOfPropertyChange(() => EnableCoreSettings);
            }
        }

        public bool LoggerConnected
        {
            get
            {
                return _loggerConnected;
            }
            set
            {
                _loggerConnected = value;
                NotifyOfPropertyChange(() => LoggerConnected);
            }
        }


        public int ProgressBarValue
        {
            get { return _progressbarValue; }
            set
            {
                _progressbarValue = value;
                NotifyOfPropertyChange(() => ProgressBarValue);
            }
        }
        #endregion // Properties

        public void SetEnableCore()
        {
            if (EnableCoreSettings == false)
            {
                EnableCoreSettings = true;
            }
            else
            {
                EnableCoreSettings = false;
            }
        }
    }
}
