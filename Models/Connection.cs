﻿using System;
using System.IO;
using System.IO.Ports;
using Caliburn.Micro;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    public class Connection
    {
        public bool connected = false;
        private int connectOption;
        private SerialPort serialPort;
        private String portName;
        private int requestSerial;
        private int[] waitSecond = new int[3] { 3, 10, 5 };
        private int[] delayMs = new int[3] { 100, 1000, 500 };
        private LoggerInfo loggerInfo;

        public Connection(String portName, int option, int serialNum, ref LoggerInfo info)
        {
            this.portName = portName;

            requestSerial = serialNum;

            connectOption = option;

            loggerInfo = info;

            if (Establish_Connection(option))
            {
                connected = true;
            }
        }

        private bool Establish_Connection(int option)
        {
            try
            {
                // USB
                if (option == 0)
                {
                    serialPort = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                }
                // RS232
                else if (option == 1)
                {
                    serialPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                }
                else if (option == 2)
                {
                    serialPort = new SerialPort(portName, 115200, Parity.None, 8, StopBits.One);
                }

                serialPort.Handshake = Handshake.None;
                serialPort.ReadBufferSize = 700000;

                serialPort.Open();
            }
            catch (Exception)
            {
                return false;
            }

            // Start Hand Shake
            if (!Start_Handshake())
            {
                serialPort.Close();
                return false;
            }

            return true;
        }

        private bool Start_Handshake()
        {
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();

            // Send SYN Packet
            if (!SendPacket(Packet.SYN_Packet()))
            {
                return false;
            }

            // Wait to get Packet
            int[] packet = Get_Packet(waitSecond[connectOption]);

            if (packet != null)
            {
                if (packet[4] == (int)Constant.Serial_flag.ACK_SYN)
                {
                    // Serial Number and Firmware Version
                    loggerInfo.Serial = Util.ByteToShort(packet[5], packet[6]);
                    loggerInfo.Firmware = Util.ByteToShort(packet[7], packet[8]);

                    if (connectOption == 0)
                    {
                        requestSerial = loggerInfo.Serial;
                    }

                    // Last Setting Date
                    try
                    {
                        loggerInfo.LastSet = new DateTime(Util.ByteToShort(packet[9], packet[10]), 
                            packet[11], packet[12], packet[13], packet[14], packet[15]);
                    }
                    catch (Exception)
                    {
                        
                    }

                    // Build Date
                    try
                    {
                        loggerInfo.BuildDate = new DateTime(Util.ByteToShort(packet[16], packet[17]), packet[18], packet[19], packet[20], packet[21], packet[22]);
                    }
                    catch (Exception)
                    {
                        
                    }

                    // Check Point Date
                    try
                    {
                        loggerInfo.CheckPoint = new DateTime(Util.ByteToShort(packet[23], packet[24]), packet[25], packet[26], packet[27], packet[28], packet[29]);
                    }
                    catch (Exception)
                    {
                        
                    }

                    // All channels Sensor Info
                    for (int i = 0; i < 8; i++)
                    {
                        loggerInfo.SensorInfos[i].Type = (byte)packet[30 + i];
                    }

                    loggerInfo.Role = (byte)packet[38];

                    // Role
                    if (loggerInfo.Role == 0)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            loggerInfo.SlaveList[i] = Util.ByteToShort(packet[39 + i * 2], packet[40 + i * 2]);
                        }
                    }
                    else // Slave Numer for Slave
                    {
                        loggerInfo.SlaveNumber = (byte)(packet[49] + 1);
                    }

                    if (!SendPacket(Packet.ACK_SYN_Packet()))
                    {
                        return false;
                    }
                    return true;
                }
            }

            serialPort.DiscardInBuffer();
            return false;
        }

        public SensorInfo Get_SensorInfo(int channel)
        {
            SensorInfo info = new SensorInfo();

            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();

            if (!SendPacket(Packet.DAT_SensorInfo_Packet(channel)))
            {
                
            }
            else
            {
                int[] packet = Get_Packet(10);

                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        info.Channel = (byte)(channel + 1);
                        info.Type = (byte)packet[5];
                        info.Settle = Util.ByteToShort(packet[6], packet[7]);
                        info.FreqMin = Util.ByteToShort(packet[8], packet[9]);
                        info.FreqMax = Util.ByteToShort(packet[10], packet[11]);
                        info.Cycle = Util.ByteToShort(packet[12], packet[13]);
                        info.Equation = (byte)packet[14];
                        for (int i = 0; i < 5; i++)
                        {
                            info.Cof[i] = Util.IntToFloat(packet[15 + i * 4],
                                packet[16 + i * 4], packet[17 + i * 4], packet[18 + i * 4]);
                        }
                        info.Compensation = (byte)packet[35];
                        info.RecordChange = Util.IntToFloat(packet[36],
                                packet[37], packet[38], packet[39]);
                        info.CounterReset = (packet[40] & (1 << channel)) > 0;
                        info.Unit = String.Format("{0}{1}{2}{3}", (char)packet[41], (char)packet[42], (char)packet[43], (char)packet[44]);

                        return info;
                    }
                }
            }
            return info;
        }

        public bool Get_AllSensors()
        {
            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.SensorInfos[i].Type != 0)
                {
                    loggerInfo.SensorInfos[i] = Get_SensorInfo(i);
                }

            }
            return true;
        }

        public bool Get_RecordInfo()
        {
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();

            if (!SendPacket(Packet.DAT_RecordInfo_Packet()))
            {
                return false;
            }
            else
            {
                int[] packet = Get_Packet(waitSecond[connectOption]);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.EnableRecord = packet[5] != 0;
                        loggerInfo.EnableSynchronize = packet[6] != 0;
                        loggerInfo.Interval = Util.ByteToShort(packet[7], packet[8]);
                        loggerInfo.EnableFastread = packet[9] != 0;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Get_RadioInfo()
        {
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();
            if (!SendPacket(Packet.DAT_RadioInfo_Packet()))
            {
                return false;
            }
            else
            {
                int[] packet = Get_Packet(10);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.RadioHP = (byte)(packet[5] - 48);
                        loggerInfo.RadioID = string.Format("{0}{1}{2}{3}", (char)packet[6], (char)packet[7],
                            (char)packet[8], (char)packet[9]);

                        return true;
                    }
                }
            }
            return false;
        }

        public bool Get_RoleInfo()
        {
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();
            if (!SendPacket(Packet.DAT_RoleInfo_Packet()))
            {
                return false;
            }
            else
            {
                int[] packet = Get_Packet(10);
                if (packet != null)
                {
                    if (Check_Valid(packet, (int)Constant.Serial_flag.DAT))
                    {
                        loggerInfo.Role = (byte)packet[5];
                        loggerInfo.SlaveNumber = (byte)packet[6];
                        loggerInfo.SlaveMode = (byte)(packet[7] - 2);
                        List<int> sl = new List<int>();
                        for (int i = 0; i < 5; i++)
                        {
                            sl.Add((int)Util.ByteToShort(packet[8 + 2 * i], packet[9 + 2 * i]));
                        }
                        loggerInfo.SlaveList = sl;

                        return true;
                    }
                }
            }

            return false;
        }

        public void Get_Menu()
        {
            SendPacket(Packet.DAT_LST_Packet());

            loggerInfo.DownloadMenu = new List<string>();

            if (Check_Start(waitSecond[connectOption]))
            {
                serialPort.ReadByte();
                serialPort.ReadByte();
                if (Util.ByteToShort(serialPort.ReadByte(), serialPort.ReadByte()) == requestSerial)
                {
                    if (serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        while (true)
                        {
                            DateTime date;
                            int year1 = serialPort.ReadByte();
                            int year2 = serialPort.ReadByte();
                            if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                            {
                                break;
                            }
                            int month = serialPort.ReadByte();
                            int day = serialPort.ReadByte();
                            date = new DateTime(Util.ByteToShort(year1, year2), month, day);

                            loggerInfo.DownloadMenu.Add(date.ToString("dd/MM/yyy"));
                        }
                        loggerInfo.DownloadMenu = loggerInfo.DownloadMenu.OrderByDescending(x => DateTime.Parse(x)).ToList();
                    }
                }
            }
        }

        public SensorInfo SendSensorInfo(SensorInfo info)
        {
            // Break when set sensor type to unused
            if (info.Type == 0)
            {
                SendPacket(Packet.SET_Sensor_Type_Packet(info.Channel - 1, info.Type));

                Thread.Sleep(delayMs[connectOption]);

                return Get_SensorInfo(info.Channel - 1);
            }


            SendPacket(Packet.SET_Sensor_Packet(info));


            Thread.Sleep(delayMs[connectOption]);

            return Get_SensorInfo(info.Channel - 1);
        }

        public void SendSensorReset()
        {
            SendPacket(Packet.SET_RESET_Packet());

            Thread.Sleep(delayMs[connectOption]);
        }

        public void SendInterval()
        {
            SendPacket(Packet.SET_Interval_Packet(loggerInfo.Interval));
        }

        public void SendEnableToggle(int option)
        {
            switch (option)
            {
                case 1:
                    SendPacket(Packet.SET_EnableRecord_Packet(Convert.ToByte(loggerInfo.EnableRecord)));
                    break;
                case 2:
                    SendPacket(Packet.SET_EnableSynchronize_Packet(Convert.ToByte(loggerInfo.EnableSynchronize)));
                    break;
                case 3:
                    SendPacket(Packet.SET_EnableFastRead_Packet(Convert.ToByte(loggerInfo.EnableFastread)));
                    break;
            }
        }

        public void SendRadioSetting()
        {
            SendPacket(Packet.SET_Radio_HPID(loggerInfo.RadioHP, loggerInfo.RadioID));
        }

        public void SendRoleSetting()
        {
            SendPacket(Packet.SET_RoleInfo_Packet(loggerInfo.Role, loggerInfo.SlaveMode + 2, loggerInfo.SlaveNumber, loggerInfo.SlaveList));
        }

        public void SendMenuReset()
        {
            SendPacket(Packet.SET_RESETSD_Packet());
        }

        public void Receive_RAW_LOG(int year, int month, int date, ref SoftwareSettings ss)
        {
            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();

            SendPacket(Packet.DAT_RED_Packet(year, month, date));

            if (Check_Start(waitSecond[connectOption]))
            {
                serialPort.ReadByte();
                serialPort.ReadByte();

                if (Util.ByteToShort(serialPort.ReadByte(), serialPort.ReadByte()) == requestSerial)
                {
                    if (serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        Directory.CreateDirectory(String.Format("DownloadData/{0}", loggerInfo.Serial.ToString()));
                        using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/DownloadData/{4}/{0}_{1}-{2}-{3}_raw.txt", loggerInfo.Serial, year, month, date, loggerInfo.Serial.ToString())))
                        {
                            int count = serialPort.ReadByte() + serialPort.ReadByte() * 256;
                            int download_count = 1;

                            file.WriteLine("Raw Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                            file.WriteLine("                      Channel 1      Channel 2      Channel 3      Channel 4      Channel 5      Channel 6      Channel 7      Channel 8");

                            while (true)
                            {
                                int year1 = serialPort.ReadByte();
                                if (year1 != Constant.STOP_BYTE && year1 != (year & 0x00ff))
                                {
                                    continue;
                                }
                                int year2 = serialPort.ReadByte();

                                if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                                {
                                    break;
                                }

                                ss.ProgressBarValue = download_count * 100 / count;

                                int month1 = serialPort.ReadByte();
                                int day1 = serialPort.ReadByte();

                                file.Write("{0:00}/{1:00}/", day1, month1);
                                file.Write("{0:0000} ", Util.ByteToShort(year1, year2));
                                file.Write("{0:00}:{1:00}:{2:00} ", serialPort.ReadByte(), serialPort.ReadByte(), serialPort.ReadByte());
                                serialPort.ReadByte();
                                for (int i = 0; i < 8; i++)
                                {
                                    byte[] floatarray = new byte[4];
                                    floatarray[0] = (byte)serialPort.ReadByte();
                                    floatarray[1] = (byte)serialPort.ReadByte();
                                    floatarray[2] = (byte)serialPort.ReadByte();
                                    floatarray[3] = (byte)serialPort.ReadByte();
                                    if (BitConverter.ToSingle(floatarray, 0) > 0.1)
                                    {
                                        file.Write(" {0: 000000.000;-000000.000} | ", Math.Round(BitConverter.ToSingle(floatarray, 0), 3));
                                    }
                                    else
                                    {
                                        file.Write("             | ");
                                    }
                                }
                                file.WriteLine();
                                download_count++;
                            }
                        }
                        ss.ProgressBarValue = 100;
                        return;
                    }
                }
            }
        }

        public void Receive_REAL_LOG(int year, int month, int date, ref SoftwareSettings ss)
        {
            List<int> recordingChannel = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.SensorInfos[i].Type != 0)
                {
                    recordingChannel.Add(i);
                }
            }

            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();

            // Get all sensor settings
            Get_AllSensors();

            SendPacket(Packet.DAT_RED_Packet(year, month, date));

            if (Check_Start(10))
            {
                serialPort.ReadByte();
                serialPort.ReadByte();
                if (Util.ByteToShort(serialPort.ReadByte(), serialPort.ReadByte()) == requestSerial)
                {
                    if (serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                    {
                        Directory.CreateDirectory(String.Format("DownloadData/{0}", loggerInfo.Serial.ToString()));
                        using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/DownloadData/{4}/{0}_{1}-{2}-{3}_real.txt", loggerInfo.Serial, year, month, date, loggerInfo.Serial.ToString())))
                        {
                            // Write Sensor Settings at the beginning of the file
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Channel {0}", i + 1).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Type: {0}", Constant.sensor_types[loggerInfo.SensorInfos[i].Type]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Unit: {0}", loggerInfo.SensorInfos[i].Unit).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Settle Time(ms): {0}", loggerInfo.SensorInfos[i].Settle).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Sweep Start(Hz): {0}", loggerInfo.SensorInfos[i].FreqMin).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Sweep Stop(Hz): {0}", loggerInfo.SensorInfos[i].FreqMax).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Cycles: {0}", loggerInfo.SensorInfos[i].Cycle).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Equation: {0}", Constant.equation_types[loggerInfo.SensorInfos[i].Equation]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^4: {0}", loggerInfo.SensorInfos[i].Cof[4]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^3: {0}", loggerInfo.SensorInfos[i].Cof[3]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^2: {0}", loggerInfo.SensorInfos[i].Cof[2]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^1: {0}", loggerInfo.SensorInfos[i].Cof[1]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);
                            foreach (int i in recordingChannel)
                            {
                                file.Write(String.Format("Coef x^0: {0}", loggerInfo.SensorInfos[i].Cof[0]).PadRight(25, ' '));
                            }
                            file.Write(Environment.NewLine);

                            int count = serialPort.ReadByte() + serialPort.ReadByte() * 256;
                            int download_count = 0;

                            file.Write(Environment.NewLine);
                            file.WriteLine("Real Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                            file.WriteLine("                      Channel 1      Channel 2      Channel 3      Channel 4      Channel 5      Channel 6      Channel 7      Channel 8");

                            Constant.Record[] records = new Constant.Record[count];

                            while (true)
                            {
                                int year1 = serialPort.ReadByte();
                                if (year1 != Constant.STOP_BYTE && year1 != (year & 0x00ff))
                                {
                                    continue;
                                }
                                int year2 = serialPort.ReadByte();

                                if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                                {
                                    break;
                                }

                                ss.ProgressBarValue = download_count * 100 / count;

                                int month1 = serialPort.ReadByte();
                                int date1 = serialPort.ReadByte();
                                int hour1 = serialPort.ReadByte();
                                int minute1 = serialPort.ReadByte();
                                int second1 = serialPort.ReadByte();
                                serialPort.ReadByte();

                                float[] r = new float[8];
                                for (int i = 0; i < 8; i++)
                                {
                                    byte[] floatarray = new byte[4];
                                    floatarray[0] = (byte)serialPort.ReadByte();
                                    floatarray[1] = (byte)serialPort.ReadByte();
                                    floatarray[2] = (byte)serialPort.ReadByte();
                                    floatarray[3] = (byte)serialPort.ReadByte();
                                    r[i] = BitConverter.ToSingle(floatarray, 0);
                                }

                                records[download_count] = new Constant.Record(Util.ByteToShort(year1, year2), month1, date1, hour1, minute1, second1, r);

                                download_count++;
                            }

                            for (int i = 0; i < download_count; i++)
                            {
                                file.Write("{0:00}/{1:00}/", records[i].day, records[i].month);
                                file.Write("{0:0000} ", records[i].year);
                                file.Write("{0:00}:{1:00}:{2:00} ", records[i].hour, records[i].minute, records[i].second);
                                float[] real = Calculate_Real_Reading(records[i].readings);
                                for (int j = 0; j < 8; j++)
                                {
                                    if (real[j] > 0.1)
                                    {
                                        file.Write(" {0: 000000.000;-000000.000} | ", Math.Round(real[j], 3));
                                    }
                                    else
                                    {
                                        file.Write("             | ");
                                    }
                                }
                                file.WriteLine();
                            }
                        }

                        ss.ProgressBarValue = 100;
                        return;
                    }
                }
            }
        }

        private float[] Calculate_Real_Reading(float[] rawReading)
        {
            float[] real = new float[8];
            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.SensorInfos[i].Compensation == 0)
                {
                    switch (loggerInfo.SensorInfos[i].Equation)
                    {
                        case 0:
                            real[i] = rawReading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(loggerInfo.SensorInfos[i].Cof, rawReading[i], 0);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhart(loggerInfo.SensorInfos[i].Cof, rawReading[i], 0);
                            break;
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                if (loggerInfo.SensorInfos[i].Compensation != 0)
                {
                    switch (loggerInfo.SensorInfos[i].Equation)
                    {
                        case 0:
                            real[i] = rawReading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(loggerInfo.SensorInfos[i].Cof, rawReading[i], real[loggerInfo.SensorInfos[i].Compensation - 1]);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhart(loggerInfo.SensorInfos[i].Cof, rawReading[i], real[loggerInfo.SensorInfos[i].Compensation - 1]);
                            break;
                    }
                }
            }

            return real;
        }

        private int[] Get_Packet(int timeOutSecond)
        {
            int[] current_packet = new int[80];
            int write_pos = 0;

            if (!Check_Start(timeOutSecond))
            {
                return null;
            }

            int current_byte = serialPort.ReadByte();
            int next_byte = -1;

            while (true)
            {
                current_packet[write_pos] = current_byte;
                write_pos++;

                if (next_byte != -1)
                {
                    current_packet[write_pos] = next_byte;
                    write_pos++;
                    next_byte = -1;
                }

                current_byte = serialPort.ReadByte();

                if (current_byte == Constant.STOP_BYTE)
                {
                    next_byte = serialPort.ReadByte();

                    if (next_byte == Constant.STOP_BYTE)
                    {
                        break;
                    }

                }
            }

            return current_packet;
        }

        private bool SendPacket(Packet.Packet_Info info)
        {
            if (serialPort != null)
            {
                try
                {
                    byte[] FROM_TO_BYTE = new byte[] { (byte)(requestSerial & 0x00ff), (byte)(requestSerial >> 8 & 0x00ff), 0, 0 };
                    serialPort.Write(Constant.START_BYTE_ARY, 0, 2);
                    serialPort.Write(FROM_TO_BYTE, 0, 4);
                    serialPort.Write(info.data, info.offset, info.count);
                    serialPort.Write(Constant.STOP_BYTE_ARY, 0, 2);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool Check_Start(int timeOutSecond)
        {
            int timer = 0;

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {

                        return false;
                    }

                    timer++;
                }
            } while (serialPort.BytesToRead == 0);

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {

                        return false;
                    }
                    timer++;
                }
            } while (serialPort.ReadByte() != Constant.START_BYTE);

            do
            {
                Thread.Sleep(5);

                if (timeOutSecond > 0)
                {
                    if (timer > 200 * timeOutSecond)
                    {

                        return false;
                    }
                    timer++;
                }
            } while (serialPort.ReadByte() != Constant.START_BYTE);

            return true;
        }

        private bool Check_Valid(int[] packet, int flag)
        {
            if (packet[0] != 0 || packet[1] != 0)
            {
                return false;
            }
            if (Util.ByteToShort(packet[2], packet[3]) != requestSerial)
            {
                return false;
            }
            if (packet[4] != flag)
            {
                return false;
            }

            return true;
        }

    }
}
