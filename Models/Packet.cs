﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    public class Packet
    {
        public struct Packet_Info
        {
            public byte[] data;
            public int offset, count;

            public Packet_Info(byte[] d, int o, int c)
            {
                data = d;
                offset = o;
                count = c;
            }
        }

        public static Packet_Info SYN_Packet()
        {
            byte[] packet = new byte[1];
            packet[0] = (byte)Constant.Serial_flag.SYN;

            Packet_Info info = new Packet_Info(packet, 0, 1);

            return info;
        }

        public static Packet_Info ACK_SYN_Packet()
        {
            byte[] packet = new byte[1];
            packet[0] = (byte)Constant.Serial_flag.ACK_SYN;

            Packet_Info info = new Packet_Info(packet, 0, 1);

            return info;
        }

        public static Packet_Info SET_Serial_Packet(int serialNum)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SER;
            packet[2] = (byte)(serialNum & 0x00ff);
            packet[3] = (byte)(serialNum >> 8 & 0x00ff);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_FW_Packet(int firmwareVersion)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.FWV;
            packet[2] = (byte)(firmwareVersion & 0x00ff);
            packet[3] = (byte)(firmwareVersion >> 8 & 0x00ff);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Time_Packet(int option, int year, int month, int day, int hour, int minute, int second)
        {
            DateTime dt = new DateTime(year, month, day, hour, minute, second);
            byte[] packet = new byte[9];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)option;
            packet[2] = (byte)(dt.Year & 0x00ff);
            packet[3] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[4] = (byte)(dt.Month);
            packet[5] = (byte)(dt.Day);
            packet[6] = (byte)(dt.Hour);
            packet[7] = (byte)(dt.Minute);
            packet[8] = (byte)(dt.Second);

            Packet_Info info = new Packet_Info(packet, 0, 9);

            return info;
        }

        public static Packet_Info SET_RTC_Packet()
        {
            DateTime dt = DateTime.Now;
            byte[] packet = new byte[13];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RTC;
            packet[2] = (byte)(dt.Year & 0x00ff);
            packet[3] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[4] = (byte)(dt.Month);
            packet[5] = (byte)(dt.Day);
            packet[6] = (byte)(dt.Hour);
            packet[7] = (byte)(dt.Minute);
            packet[8] = (byte)(dt.Second);

            Packet_Info info = new Packet_Info(packet, 0, 9);

            return info;
        }

        public static Packet_Info SET_Sensor_Type_Packet(int channel, int type)
        {
            byte[] packet = new byte[8];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_TYPE;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(type);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_VW_Packet(int flag, int channel, int data)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)flag;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(data & 0x00ff);
            packet[4] = (byte)(data >> 8 & 0x00ff);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Sensor_Equation_Packet(int channel, int equation)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_EQUATION;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(equation);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Sensor_COF_Packet(int channel, int cof, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[8];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COF;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(cof);
            packet[4] = f[0];
            packet[5] = f[1];
            packet[6] = f[2];
            packet[7] = f[3];

            Packet_Info info = new Packet_Info(packet, 0, 8);

            return info;
        }

        public static Packet_Info SET_Interval_Packet(int interval)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_INTERVAL;
            packet[2] = (byte)(interval & 0x00ff);
            packet[3] = (byte)(interval >> 8 & 0x00ff);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_RESET_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RESET_SENSOR;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info SET_RESETSD_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RESET_SD;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info SET_MODE_Packet(int mode)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.MODE;
            packet[2] = (byte)mode;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_Role_Packet(int role)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ROLE;
            packet[2] = (byte)role;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_Counter_Reset_Packet(int channel, int reset)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.COUNTER_RESET;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(reset);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Sample_Change_Packet(int channel, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SAMPLE_CHANGE;
            packet[2] = (byte)(channel);
            packet[3] = f[0];
            packet[4] = f[1];
            packet[5] = f[2];
            packet[6] = f[3];

            Packet_Info info = new Packet_Info(packet, 0, 7);

            return info;
        }

        public static Packet_Info SET_Compensated_Packet(int channel, int enable)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COMP;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(enable);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Compensation_Packet(int channel, int sourec)
        {
            byte[] packet = new byte[4];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_COMPCHAN;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(sourec);

            Packet_Info info = new Packet_Info(packet, 0, 4);

            return info;
        }

        public static Packet_Info SET_Unit_Packet(int channel, string unit)
        {
            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR_UNIT;
            packet[2] = (byte)(channel);
            packet[3] = (byte)(unit[0]);
            packet[4] = (byte)(unit[1]);
            packet[5] = (byte)(unit[2]);
            packet[6] = (byte)(unit[3]);

            Packet_Info info = new Packet_Info(packet, 0, 7);

            return info;
        }

        public static Packet_Info SET_Slave_List_Packet(int index, int serial)
        {
            byte[] packet = new byte[5];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SLAVE_LIST;
            packet[2] = (byte)(index);
            packet[3] = (byte)(serial & 0x00ff);
            packet[4] = (byte)(serial >> 8 & 0x00ff);

            Packet_Info info = new Packet_Info(packet, 0, 5);

            return info;
        }

        public static Packet_Info SET_Slave_Number_Packet(int slave_number)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SLAVE_NUMBER;
            packet[2] = (byte)slave_number;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_Sensor_Packet(SensorInfo info)
        {
            byte[] packet = new byte[43];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SENSOR;
            packet[2] = (byte)(info.Channel - 1);
            packet[3] = info.Type;
            packet[4] = (byte)(info.Unit[0]);
            packet[5] = (byte)(info.Unit[1]);
            packet[6] = (byte)(info.Unit[2]);
            packet[7] = (byte)(info.Unit[3]);
            packet[8] = (byte)(info.Settle & 0x00ff);
            packet[9] = (byte)(info.Settle >> 8 & 0x00ff);
            packet[10] = (byte)(info.Cycle & 0x00ff);
            packet[11] = (byte)(info.Cycle >> 8 & 0x00ff);
            packet[12] = (byte)(info.FreqMin & 0x00ff);
            packet[13] = (byte)(info.FreqMin >> 8 & 0x00ff);
            packet[14] = (byte)(info.FreqMax & 0x00ff);
            packet[15] = (byte)(info.FreqMax >> 8 & 0x00ff);
            packet[16] = (byte)(info.Equation);
            for (int i = 0; i < 5; i++)
            {
                byte[] f1 = BitConverter.GetBytes(info.Cof[i]);
                packet[17 + i * 4] = f1[0];
                packet[18 + i * 4] = f1[1];
                packet[19 + i * 4] = f1[2];
                packet[20 + i * 4] = f1[3];
            }
            packet[37] = (byte)(info.Compensation);
            packet[38] = Convert.ToByte(info.CounterReset);
            byte[] f2 = BitConverter.GetBytes(info.RecordChange);
            packet[39] = f2[0];
            packet[40] = f2[1];
            packet[41] = f2[2];
            packet[42] = f2[3];

            Packet_Info rs = new Packet_Info(packet, 0, 43);

            return rs;
        }

        public static Packet_Info SET_EnableRecord_Packet(int enable)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ENABLE_RECORD;
            packet[2] = (byte)enable;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_EnableSynchronize_Packet(int enable)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ENABLE_SYNCHRONIZE;
            packet[2] = (byte)enable;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_EnableFastRead_Packet(int enable)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ENABLE_FASTREAD;
            packet[2] = (byte)enable;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info SET_Record_Packet(int enable_record, int enable_synchronize, int measure_interval, int enableFastread, float[] roc_values)
        {
            byte[] packet = new byte[39];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RECORD;
            packet[2] = (byte)(enable_record);
            packet[3] = (byte)(enable_synchronize);
            packet[4] = (byte)(measure_interval & 0x00ff);
            packet[5] = (byte)(measure_interval >> 8 & 0x00ff);
            packet[6] = (byte)(enableFastread);
            for (int i = 0; i < 8; i++)
            {
                byte[] f = BitConverter.GetBytes(roc_values[i]);
                packet[7 + i * 4] = f[0];
                packet[8 + i * 4] = f[1];
                packet[9 + i * 4] = f[2];
                packet[10 + i * 4] = f[3];
            }

            Packet_Info info = new Packet_Info(packet, 0, 39);

            return info;
        }

        public static Packet_Info SET_RoleInfo_Packet(int role, int slaveMode, int slaveNumber, List<int> slaveList)
        {
            byte[] packet = new byte[15];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.ROLE_INFO;
            packet[2] = (byte)(role);
            packet[3] = (byte)(slaveMode);
            packet[4] = (byte)(slaveNumber);
            for (int i = 0; i < 5; i++)
            {
                packet[5 + i * 2] = (byte)(slaveList[i] & 0x00ff);
                packet[6 + i * 2] = (byte)(slaveList[i] >> 8 & 0x00ff);
            }

            Packet_Info info = new Packet_Info(packet, 0, 15);

            return info;
        }

        public static Packet_Info SET_SystemInfo_Packet(int serialNumber, int firmwareVersion, DateTime buildDate, DateTime lastSetDate, DateTime checkPoint, int mode)
        {
            byte[] packet = new byte[28];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.SYSTEM_INFO;
            packet[2] = (byte)(serialNumber & 0x00ff);
            packet[3] = (byte)(serialNumber >> 8 & 0x00ff);
            packet[4] = (byte)(firmwareVersion & 0x00ff);
            packet[5] = (byte)(firmwareVersion >> 8 & 0x00ff);
            packet[6] = (byte)(buildDate.Year & 0x00ff);
            packet[7] = (byte)(buildDate.Year >> 8 & 0x00ff);
            packet[8] = (byte)(buildDate.Month);
            packet[9] = (byte)(buildDate.Day);
            packet[10] = (byte)(buildDate.Hour);
            packet[11] = (byte)(buildDate.Minute);
            packet[12] = (byte)(buildDate.Second);
            packet[13] = (byte)(lastSetDate.Year & 0x00ff);
            packet[14] = (byte)(lastSetDate.Year >> 8 & 0x00ff);
            packet[15] = (byte)(lastSetDate.Month);
            packet[16] = (byte)(lastSetDate.Day);
            packet[17] = (byte)(lastSetDate.Hour);
            packet[18] = (byte)(lastSetDate.Minute);
            packet[19] = (byte)(lastSetDate.Second);
            packet[20] = (byte)(checkPoint.Year & 0x00ff);
            packet[21] = (byte)(checkPoint.Year >> 8 & 0x00ff);
            packet[22] = (byte)(checkPoint.Month);
            packet[23] = (byte)(checkPoint.Day);
            packet[24] = (byte)(checkPoint.Hour);
            packet[25] = (byte)(checkPoint.Minute);
            packet[26] = (byte)(checkPoint.Second);
            packet[27] = (byte)(mode);

            Packet_Info info = new Packet_Info(packet, 0, 28);

            return info;
        }

        public static Packet_Info SET_Radio_HPID(int hp, string id)
        {
            byte[] packet = new byte[7];
            packet[0] = (byte)Constant.Serial_flag.SET;
            packet[1] = (byte)Constant.Set_flag.RADIO_HPID;
            packet[2] = (byte)(hp);
            packet[3] = (byte)(id[0]);
            packet[4] = (byte)(id[1]);
            packet[5] = (byte)(id[2]);
            packet[6] = (byte)(id[3]);

            Packet_Info info = new Packet_Info(packet, 0, 7);

            return info;
        }

        public static Packet_Info DAT_RTM_Packet(int rtm)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RTM;
            packet[2] = (byte)(rtm);

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info DAT_RED_Packet(int year, int month, int day)
        {
            byte[] packet = new byte[6];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RED;
            packet[2] = (byte)(year & 0x00ff);
            packet[3] = (byte)(year >> 8 & 0x00ff);
            packet[4] = (byte)month;
            packet[5] = (byte)day;

            Packet_Info info = new Packet_Info(packet, 0, 6);

            return info;
        }

        public static Packet_Info DAT_LST_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.LST;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info DAT_SensorInfo_Packet(int channel)
        {
            byte[] packet = new byte[3];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.SENSOR_INFO;
            packet[2] = (byte)channel;

            Packet_Info info = new Packet_Info(packet, 0, 3);

            return info;
        }

        public static Packet_Info DAT_SLST_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.SLST;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info DAT_RecordInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RECORD_INFO;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info DAT_RoleInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.ROLE_INFO;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }

        public static Packet_Info DAT_RadioInfo_Packet()
        {
            byte[] packet = new byte[2];
            packet[0] = (byte)Constant.Serial_flag.DAT;
            packet[1] = (byte)Constant.Dat_flag.RADIO_DAT;

            Packet_Info info = new Packet_Info(packet, 0, 2);

            return info;
        }
    }
}
