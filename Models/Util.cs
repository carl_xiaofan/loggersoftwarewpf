﻿using System;

namespace LoggerSoftware.Models
{
    public class Util
    {
        public static bool ValidInt(string number, int min, int max)
        {
            int res;
            if ((!Int32.TryParse(number, out res)) || ((min > Int32.Parse(number) || max < Int32.Parse(number))))
            {
                return false;
            }
            return true;
        }

        public static bool ValidFloat(string number)
        {
            float res;
            if ((!float.TryParse(number, out res)))
            {
                return false;
            }
            return true;
        }

        public static float IntToFloat(int i1, int i2, int i3, int i4)
        {
            byte[] floatarray = new byte[4];
            floatarray[0] = (byte)i1;
            floatarray[1] = (byte)i2;
            floatarray[2] = (byte)i3;
            floatarray[3] = (byte)i4;

            return BitConverter.ToSingle(floatarray, 0);
        }

        public static ushort ByteToShort(int i1, int i2)
        {
            return BitConverter.ToUInt16(new byte[2] { (byte)i1, (byte)i2 }, 0);
        }
    }
}
