﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    public class SensorInfo : Screen
    {
        #region Const
        #endregion // Const

        #region Data Members
        // ----------------------------------------------------------------------
        private byte _channel;
        private byte _type;
        private int _settle;
        private int _freqMin;
        private int _freqMax;
        private int _cycle;
        private byte _equation;
        private byte _compensation;
        private bool _counterReset;
        private string _unit;
        private float _recordChange;
        private List<float> _cof;
        // ----------------------------------------------------------------------
        #endregion // Data Members

        #region Constructor
        public SensorInfo()
        {
            Type = 0;
            Settle = 40;
            FreqMax = 1600;
            FreqMin = 3200;
            Cycle = 250;
            Equation = 0;
            Compensation = 0;
            RecordChange = 0;
            CounterReset = false;
            Unit = "Null";
            Cof = new List<float>(new float[5]);
        }

        public SensorInfo(SensorInfo info)
        {
            Channel = info.Channel;
            Type = info.Type;
            Settle = info.Settle;
            FreqMax = info.FreqMax;
            FreqMin = info.FreqMin;
            Cycle = info.Cycle;
            Equation = info.Equation;
            Compensation = info.Compensation;
            RecordChange = info.RecordChange;
            CounterReset = info.CounterReset;
            Unit = info.Unit;
            Cof = info.Cof;
        }
        #endregion // Constructor

        #region Properties
        public byte Channel
        {
            get
            {
                return _channel;
            }
            set
            {
                _channel = value;
                NotifyOfPropertyChange(() => Channel);
            }
        }

        public byte Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                NotifyOfPropertyChange(() => Type); 
                NotifyOfPropertyChange(() => TypeName);
                NotifyOfPropertyChange(() => IsEnabled);
            }
        }

        public int Settle
        {
            get
            {
                return _settle;
            }
            set
            {
                _settle = value;
                NotifyOfPropertyChange(() => Settle);
            }
        }

        public int FreqMin
        {
            get
            {
                return _freqMin;
            }
            set
            {
                _freqMin = value;
                NotifyOfPropertyChange(() => FreqMin);
            }
        }

        public int FreqMax
        {
            get
            {
                return _freqMax;
            }
            set
            {
                _freqMax = value;
                NotifyOfPropertyChange(() => FreqMax);
            }
        }

        public int Cycle
        {
            get
            {
                return _cycle;
            }
            set
            {
                _cycle = value;
                NotifyOfPropertyChange(() => Cycle);
            }
        }

        public byte Equation
        {
            get
            {
                return _equation;
            }
            set
            {
                _equation = value;
                NotifyOfPropertyChange(() => Equation);
                NotifyOfPropertyChange(() => EquationName);
            }
        }

        public byte Compensation
        {
            get
            {
                return _compensation;
            }
            set
            {
                _compensation = value;
                NotifyOfPropertyChange(() => Compensation);
            }
        }

        public bool CounterReset
        {
            get
            {
                return _counterReset;
            }
            set
            {
                _counterReset = value;
                NotifyOfPropertyChange(() => CounterReset);
            }
        }

        public string Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                _unit = value;
                NotifyOfPropertyChange(() => Unit);
            }
        }

        public float RecordChange
        {
            get
            {
                return _recordChange;
            }
            set
            {
                _recordChange = value;
                NotifyOfPropertyChange(() => RecordChange);
            }
        }

        public List<float> Cof
        {
            get
            {
                return _cof;
            }
            set
            {
                _cof = value;
                NotifyOfPropertyChange(() => Cof);
            }
        }
        #endregion // Properties

        #region DisplayforDataGrid
        public string TypeName
        {
            get
            {
                return Constant.sensor_types[_type];
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _type != 0;
            }
        }

        public string EquationName
        {
            get
            {
                return Constant.equation_types[_equation];
            }
        }
        #endregion // DisplayforDataGrid
    }
}
