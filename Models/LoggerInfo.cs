﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSoftware.Models
{
    public class LoggerInfo : Screen
    {
        #region Const
        public const byte MAX_CHANNEL = 8;
        public const byte MAX_SLAVE = 5;
        #endregion // Const

        #region Data Members
        // ----------------------------------------------------------------------
        private ushort _serial;
        private ushort _firmware;
        private byte _role;
        private byte _mode;
        private byte _slaveMode;  
        private byte _slaveNumber;
        private bool _enableRecord;
        private bool _enableSynchronize;
        private bool _enableFastread;
        private byte _radioHP;
        private string _radioID;
        private ushort _interval;

        private DateTime _lastSet;
        private DateTime _buildDate;
        private DateTime _checkPoint;
        private DateTime _currentTime;
        private List<int> _slaveList;
        private List<string> _downloadMenu;
        private BindableCollection<SensorInfo> _sensorInfos;

        // ----------------------------------------------------------------------
        #endregion // Data Members

        #region Constructor
        public LoggerInfo()
        {
            Serial = 0;
            Firmware = 0;
            Role = 0;
            Mode = 0;
            SlaveMode = 0;
            SlaveNumber = 0;
            EnableRecord = false;
            EnableSynchronize = false;
            EnableFastread = false;
            RadioHP = 0;
            RadioID = "0000";
            Interval = 0;
            LastSet = DateTime.MinValue;
            BuildDate = DateTime.MinValue;
            CheckPoint = DateTime.MinValue;
            SlaveList = new List<int>(new int[5]);
            DownloadMenu = new List<string>();
            SensorInfos = new BindableCollection<SensorInfo>();
            for (var i = 0; i < MAX_CHANNEL; i++)
            {
                SensorInfos.Add(new SensorInfo());
                SensorInfos[i].Channel = (byte)(i + 1);
            }
        }
        #endregion // Constructor

        #region Properties
        public ushort Serial
        {
            get
            {
                return _serial;
            }
            set
            {
                _serial = value;
                NotifyOfPropertyChange(() => Serial);
            }
        }

        public ushort Firmware
        {
            get
            {
                return _firmware;
            }
            set
            {
                _firmware = value;
                NotifyOfPropertyChange(() => Firmware);
            }
        }

        public byte Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
                NotifyOfPropertyChange(() => Role);
            }
        }

        public byte Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                NotifyOfPropertyChange(() => Mode);
            }
        }

        public byte SlaveMode
        {
            get
            {
                return (byte)(_slaveMode - 2);
            }
            set
            {
                _slaveMode = (byte)(value + 2);
                NotifyOfPropertyChange(() => SlaveMode);
            }
        }

        public byte SlaveNumber
        {
            get
            {
                return _slaveNumber;
            }
            set
            {
                _slaveNumber = value;
                NotifyOfPropertyChange(() => SlaveNumber);
            }
        }

        public bool EnableRecord
        {
            get
            {
                return _enableRecord;
            }
            set
            {
                _enableRecord = value;
                NotifyOfPropertyChange(() => EnableRecord);
            }
        }

        public bool EnableSynchronize
        {
            get
            {
                return _enableSynchronize;
            }
            set
            {
                _enableSynchronize = value;
                NotifyOfPropertyChange(() => EnableSynchronize);
            }
        }

        public bool EnableFastread
        {
            get
            {
                return _enableFastread;
            }
            set
            {
                _enableFastread = value;
                NotifyOfPropertyChange(() => EnableFastread);
            }
        }

        public byte RadioHP
        {
            get
            {
                return _radioHP;
            }
            set
            {
                _radioHP = value;
                NotifyOfPropertyChange(() => RadioHP);
            }
        }

        public string RadioID
        {
            get
            {
                return _radioID;
            }
            set
            {
                _radioID = value;
                NotifyOfPropertyChange(() => RadioID);
            }
        }

        public DateTime LastSet
        {
            get
            {
                return _lastSet;
            }
            set
            {
                _lastSet = value;
                NotifyOfPropertyChange(() => LastSet);
            }
        }

        public DateTime BuildDate
        {
            get
            {
                return _buildDate;
            }
            set
            {
                _buildDate = value;
                NotifyOfPropertyChange(() => BuildDate);
            }
        }

        public DateTime CheckPoint
        {
            get
            {
                return _checkPoint;
            }
            set
            {
                _checkPoint = value;
                NotifyOfPropertyChange(() => CheckPoint);
            }
        }

        public DateTime CurrentTime
        {
            get
            {
                return _currentTime;
            }
            set
            {
                _currentTime = value;
                NotifyOfPropertyChange(() => CurrentTime);
            }
        }

        public List<int> SlaveList
        {
            get
            {
                return _slaveList;
            }
            set
            {
                _slaveList = value;
                NotifyOfPropertyChange(() => SlaveList);
            }
        }

        public List<string> DownloadMenu
        {
            get
            {
                return _downloadMenu;
            }
            set
            {
                _downloadMenu = value;
                NotifyOfPropertyChange(() => DownloadMenu);
            }
        }

        public BindableCollection<SensorInfo> SensorInfos
        {
            get
            {
                return _sensorInfos;
            }
            set
            {
                _sensorInfos = value;
                NotifyOfPropertyChange(() => SensorInfos);
            }
        }

        public ushort Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                _interval = value;
                NotifyOfPropertyChange(() => Interval);
            }
        }
        #endregion // Properties
    }
}
